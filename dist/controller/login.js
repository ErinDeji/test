var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const axios = require("axios");
const tough = require("tough-cookie");
const axiosCookieJarSupport = require("axios-cookiejar-support").default;
const cookieJar = new tough.CookieJar();
exports.login = (req, res) => __awaiter(this, void 0, void 0, function* () {
    const { username, password } = req.body;
    const data = {
        username,
        password,
    };
    try {
        axios
            .post("https://sso.shoplineapp.com/users/sign_in", data, {
            withCredentials: true,
            jar: cookieJar,
        })
            .then((response) => __awaiter(this, void 0, void 0, function* () {
            const data = yield getOrders();
            console.log(data);
            return;
        }))
            .catch((error) => {
            console.log(error);
        });
    }
    catch (error) {
        console.log(error);
        return error;
    }
});
const getOrders = () => __awaiter(this, void 0, void 0, function* () {
    return yield axios
        .get("https://admin.shoplineapp.com/api/admin/v1/limhoho2008910/orders?limit=100", {
        jar: cookieJar,
        withCredentials: true,
    })
        .then((res) => {
        return res;
    })
        .catch((err) => {
        return err;
    });
    // axios({
    //     method: "get",
    //     url: "https://admin.shoplineapp.com/api/admin/v1/limhoho2008910/orders?limit=100",
    //   }).then(
    //     (response) => {
    //       console.log(response);
    //     }
    //   ).catch(error => {
    //       console.log(error);
    //   });
    // axios.get('https://admin.shoplineapp.com/api/admin/v1/limhoho2008910/orders?limit=100')
    //   .then(response => {
    //     res.json(response.status)
    //   });
});
//# sourceMappingURL=login.js.map