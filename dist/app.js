"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
require("dotenv").config();
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const routes = require("./routes/index");
const app = express_1.default();
app.use(cookieParser());
app.use(bodyParser.json());
app.use("/api", routes);
const port = process.env.PORT;
app.get("/", (req, res) => {
    res.send("I hope you know what you are doing!");
});
app.listen(port, () => {
    console.log(`running on port ${port}`);
});
//# sourceMappingURL=app.js.map