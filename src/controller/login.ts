const axios: any = require("axios");
const tough: any = require("tough-cookie");
const axiosCookieJarSupport: any = require("axios-cookiejar-support").default;

const cookieJar: any = new tough.CookieJar();

/**
 * login controller
 * login to axios url through defined route and get back auth user credentials
 * pass cookie credentials to order to access order json
 */

exports.login = async (
  req: any,
  res: any
): Promise<{ getOrders: () => Promise<any> }> => {
  interface LoginInterface {
    username: string;
    password: string;
  }

  const { username, password } = req.body;
  const data: LoginInterface = {
    username,
    password,
  };
  try {
    axios
      .post("https://sso.shoplineapp.com/users/sign_in", data, {
        withCredentials: true,
        jar: cookieJar,
      })
      .then(
        async (response): Promise<object> => {
          // order json not coming in because unable to get cookie
          const data = await getOrders()
          console.log(data);
          return;
        }
      )
      .catch((error) => {
        console.log(error);
      });
  } catch (error) {
    console.log(error);
    return error;
  }
};


/**
 * get orders controller
 * get orders by merchant id
 * merchant id will be provided after login
 * can access order json with appropriate user cookie
 */

const getOrders = async (): Promise<object> => {
  return await axios
    .get(
      "https://admin.shoplineapp.com/api/admin/v1/limhoho2008910/orders?limit=100",
      {
        jar: cookieJar,
        withCredentials: true,
      }
    )
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err;
    });
  // axios({
  //     method: "get",
  //     url: "https://admin.shoplineapp.com/api/admin/v1/limhoho2008910/orders?limit=100",
  //   }).then(
  //     (response) => {
  //       console.log(response);
  //     }
  //   ).catch(error => {
  //       console.log(error);
  //   });
  // axios.get('https://admin.shoplineapp.com/api/admin/v1/limhoho2008910/orders?limit=100')
  //   .then(response => {
  //     res.json(response.status)
  //   });
};
