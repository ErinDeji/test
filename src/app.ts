import express from "express";
require("dotenv").config();

const bodyParser: any = require("body-parser");
const cookieParser: any = require("cookie-parser");
const routes: any = require("./routes/index");

const app = express();
app.use(cookieParser());
app.use(bodyParser.json());

app.use("/api", routes);

const port = process.env.PORT;
app.get("/", (req: any, res: any): void => {
  res.send("status okay! :)");
});

app.listen(port, (): void => {
  console.log(`running on port ${port}`);
});
