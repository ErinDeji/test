const express: any = require("express");
const router: any = express.Router();

/**
 * login route
 */
const { login } = require("../controller/login");

router.post("/login", login);

module.exports = router;
